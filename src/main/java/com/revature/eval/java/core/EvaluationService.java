package com.revature.eval.java.core;

import java.time.temporal.Temporal;
import java.util.*;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		String reversed_string = "";
		for(int i = 0; i < string.length(); i++){
			reversed_string = reversed_string + string.charAt(string.length() - 1 - i);
		}

		return reversed_string;
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */


	public String acronym(String phrase) {
		// TODO Write an implementation for this method declaration

		StringBuilder result = new StringBuilder();

		for(int i = 0; i < phrase.length(); i++){
			if(i == 0) {
				result.append(phrase.charAt(0));
			} else {
				boolean adding_letter = false;
				while(!Character.isAlphabetic(phrase.charAt(i)) && i <= (phrase.length() - 1)) {
					adding_letter = true;
					i++;
				}
				if(adding_letter) result.append(Character.toUpperCase(phrase.charAt(i)));
			}
		}
		return result.toString();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			// TODO Write an implementation for this method declaration
			if(this.sideOne == this.sideTwo && this.sideOne == this.sideThree) return true;
			return false;
		}

		public boolean isIsosceles() {
			// TODO Write an implementation for this method declaration
			if(this.sideOne == this.sideTwo || this.sideOne == this.sideThree ||
					this.sideTwo == this.sideThree) return true;
			return false;
		}

		public boolean isScalene() {
			// TODO Write an implementation for this method declaration
			if(this.sideOne != this.sideTwo && this.sideOne != this.sideThree &&
					this.sideTwo != this.sideThree) return true;
			return false;
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 *
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */

	public int getScrabbleScore(String string) {
		// TODO Write an implementation for this method declaration
		char[] value_one = {'A', 'E', 'I','L', 'N', 'O', 'R', 'S', 'T', 'U'};
		char[] value_two = {'D', 'G'};
		char[] value_three = {'B', 'C', 'M', 'P'};
		char[] value_four = {'F', 'H', 'V', 'W', 'Y'};
		char[] value_five = {'K'};
		char[] value_eight = {'J', 'X'};
		char[] value_ten = {'Q', 'Z'};


		int score = 0;
		string = string.toUpperCase();



		for(int i = 0; i < string.length(); i++){
			if(searchCharArray(value_one, string.charAt(i))) {
				score += 1;
			} else if(searchCharArray(value_two, string.charAt(i))){
				score += 2;
			} else if(searchCharArray(value_three, string.charAt(i))){
				score += 3;
			} else if(searchCharArray(value_four, string.charAt(i))){
				score += 4;
			} else if(searchCharArray(value_five, string.charAt(i))){
				score += 5;
			} else if(searchCharArray(value_eight, string.charAt(i))){
				score += 8;
			} else if(searchCharArray(value_ten, string.charAt(i))){
				score += 10;
			}
		}

		return score;
	}
	public boolean searchCharArray(char[] array, char searchChar){
		for (char c : array) {
			if (c == searchChar) {
				return true;
			}
		}
		return false;
	}

	public int findCharIndex(char[] array, char searchChar){
		for (int i = 0; i < array.length; i++) {
			if (array[i] == searchChar) {
				return i;
			}
		}
		return -1;
	}

	public boolean searchStringArray(String[] array, String searchString){
		for (String s : array) {
			if (s.equals(searchString)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration

		StringBuilder result = new StringBuilder();
		boolean countryCodeTakenCareOf = false;
		char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		char[] legalCharacters = {'+', '-', '.', ' ', '(', ')'};

		for(int i = 0; i < string.length(); i++){
			char currentChar = string.charAt(i);
			if(!countryCodeTakenCareOf && currentChar == '1'){
				countryCodeTakenCareOf = true;
				continue;
			}
			if(!searchCharArray(numbers, currentChar) && !searchCharArray(legalCharacters, currentChar)){
				throw new IllegalArgumentException();
			}
			if(searchCharArray(numbers, currentChar)){
				result.append(currentChar);
			}
		}

		if(result.length() > 10){
			throw new IllegalArgumentException();
		}
		return result.toString();
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		// TODO Write an implementation for this method declaration

		Map<String, Integer> resultMap = new HashMap<String, Integer>();
		char[] letters = {'A', 'a', 'E', 'e', 'I', 'i','L', 'l', 'N',
				'n', 'O', 'o', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u',
				'D', 'd', 'G', 'g', 'B', 'b', 'C', 'c', 'M', 'm', 'P',
				'p', 'F', 'f', 'H', 'h', 'V', 'v', 'W', 'w', 'Y', 'y',
				'K', 'k', 'J', 'j', 'X', 'x', 'Q', 'q', 'Z', 'z'};

		StringBuilder current = new StringBuilder();
		for(int i = 0; i < string.length(); i++){

			if(searchCharArray(letters, string.charAt(i))){
				current.append(string.charAt(i));
			} else{
				if(!resultMap.containsKey(current.toString()) && !current.toString().equals("")){
					resultMap.put(current.toString(), 1);
				} else if(!current.toString().equals("")) {
					int newValue = resultMap.get(current.toString()) + 1;
					resultMap.replace(current.toString(), newValue);
				}
				current = new StringBuilder();
			}
		}

		if(!resultMap.containsKey(current.toString()) && !current.toString().equals("")){
			resultMap.put(current.toString(), 1);
		} else if(!current.toString().equals("")){
			int newValue = resultMap.get(current.toString()) + 1;
			resultMap.replace(current.toString(), newValue);
		}


		return resultMap;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T extends Comparable<T>> {
		private List<T> sortedList;

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			double lowerBound = 0;
			double upperBound = sortedList.size();
			int currentIndex = (int) Math.floor((lowerBound + upperBound) / 2);
			boolean foundIt = false;
			while(!foundIt){

				if(sortedList.get(currentIndex).equals(t)){
					foundIt = true;
				} else {
					if(sortedList.get(currentIndex).compareTo(t) < 0){
						lowerBound = currentIndex;
						currentIndex = (int) Math.floor((lowerBound + upperBound) / 2);
					} else if(sortedList.get(currentIndex).compareTo(t) > 0){
						upperBound = currentIndex;
						currentIndex = (int) Math.floor((lowerBound + upperBound) / 2);
					}
				}
			}

			return currentIndex;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		char[] consonants = {'L', 'l', 'N', 'n', 'R', 'r',
				'D', 'd', 'G', 'g', 'B', 'b', 'C', 'c', 'M',
				'm', 'P', 'p', 'F', 'f', 'H', 'h', 'V', 'v',
				'W', 'w', 'Y', 'y', 'K', 'k', 'J', 'j', 'X',
				'x',  'Z', 'z'};
		char [] specialStartingChars = {'S', 's', 'T', 't','Q', 'q'};
		String[] specialSounds = {"Sh", "sh", "Sch", "sch", "Th", "th", "Qu", "qu"};
		char [] vowels = {'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'};

		boolean atFirstLetter = true;
		StringBuilder resultString = new StringBuilder();
		for(int i = 0; i < string.length(); i++){
			if(atFirstLetter){

				if(searchCharArray(consonants, string.charAt(i))){
					for(int j = i + 1; j < string.length(); j++){
						if(string.charAt(j) != ' '){
							resultString.append(string.charAt(j));
						} else{
							break;
						}
					}
					resultString.append(string.charAt(i)).append("ay");
				}

				if(searchCharArray(specialStartingChars, string.charAt(i))) {
					if (searchStringArray(specialSounds, string.substring(i, i + 2))) {
						for (int j = i + 2; j < string.length(); j++) {
							if(string.charAt(j) != ' '){
								resultString.append(string.charAt(j));
							} else{
								break;
							}
						}
						resultString.append(string, i, i + 2).append("ay");
					} else if (searchStringArray(specialSounds, string.substring(i, i + 3))) {
						for (int j = i + 3; j < string.length(); j++) {
							if(string.charAt(j) != ' '){
								resultString.append(string.charAt(j));
							} else{
								break;
							}
						}
						resultString.append(string, i, i + 3).append("ay");
					}
				}

				if(searchCharArray(vowels, string.charAt(i))){
					for(int j = i; j < string.length(); j++){
						if(string.charAt(j) != ' '){
							resultString.append(string.charAt(j));
						} else {
							break;
						}
					}
					resultString.append("ay");
				}

				atFirstLetter = false;
			}

			if(string.charAt(i) == ' '){
				resultString.append(' ');
				atFirstLetter = true;
			}
		}
		return resultString.toString();
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		int numDigits = 0;
		int temp = input;
		List <Integer> digitsArray = new ArrayList<Integer>();

		while(temp > 0){
			digitsArray.add(temp % 10);
			temp = temp / 10;
			numDigits++;
		}

		int compareNum = 0;
		for(int i = 0; i < digitsArray.size(); i++){
			compareNum += Math.pow(digitsArray.get(i), numDigits);
		}
		return compareNum == input;
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		List<Long> primeList = new ArrayList<Long>();
		if(l == 2L){
			primeList.add(2L);
		} else if(l == 3L){
			primeList.add(3L);
		}

		for(long i = 2; i * i <= l; i++){
			if(l % i == 0){
				boolean isPrime = true;
				for (int j = 2; j < i; j++) {
					if (i % j == 0) {
						isPrime = false;
						break;
					}
				}

				if(isPrime) {
					primeList.add(i);
					primeList.addAll(calculatePrimeFactorsOf(i));
				}
			}
		}
		return primeList;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public int searchCharArray(char[] array, char searchChar){
			for (int i = 0; i < array.length; i++) {
				if (array[i] == searchChar) {
					return i;
				}
			}
			return -1;
		}

		public String rotate(String string) {
			// TODO Write an implementation for this method declaration
			char[] letters = {'A', 'a', 'B', 'b', 'C', 'c', 'D',
					'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h',
					'I', 'i', 'J', 'j', 'K', 'k','L', 'l', 'M',
					'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q',
					'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V',
					'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'};
			StringBuilder resultString = new StringBuilder();

			for(int i = 0; i < string.length(); i++){
				int currentLetterPlace = searchCharArray(letters, string.charAt(i));
				if(currentLetterPlace >= 0){

					char newChar = letters[(currentLetterPlace + key * 2) % 52];
					resultString.append(newChar);
				} else{
					resultString.append(string.charAt(i));
				}
			}


			return resultString.toString();
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i) {
		// TODO Write an implementation for this method declaration
		if(i == 0) throw new IllegalArgumentException();
		if(i == 1) return 2;
		if(i == 2) return 3;
		List<Integer> primes = new ArrayList<Integer>();

		primes.add(2);
		primes.add(3);

		int j = 4;
		while(primes.size() < i){
			boolean isPrime = true;
			for (Integer prime : primes) {
				if (j % prime == 0) {
					isPrime = false;
					break;
				}
			}
			if(isPrime) primes.add(j);
			j++;
		}
		return primes.get(primes.size() - 1);
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			char[] letters = {'A', 'a', 'B', 'b', 'C', 'c', 'D',
					'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h',
					'I', 'i', 'J', 'j', 'K', 'k','L', 'l', 'M',
					'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q',
					'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V',
					'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'};
			char[] reverseLetters = {'Z','z','Y','y','X','x','W',
					'w','V','v','U','u','T','t','S','s','R','r', 'Q','q',
					'P','p','O','o','N','n','M','m','L', 'l','K','k','J',
					'j','I','i','H','h','G','g', 'F','f', 'E', 'e', 'D',
					'd', 'C', 'c', 'B', 'b', 'A', 'a'};

			char[] numbers = {'0', '1', '2', '3', '4' ,'5', '6', '7', '8', '9'};

			StringBuilder resultString = new StringBuilder();
			int spaceTracker = 0;
			boolean addedSpace = false;
			for(int i = 0; i < string.length(); i++){
				int currentCharPlace = searchCharArray(letters, string.charAt(i));

				if(currentCharPlace >= 0){
					//Checks for lowercase letters
					if(currentCharPlace % 2 == 0){
						resultString.append(reverseLetters[currentCharPlace + 1]);
					} else {
						resultString.append(reverseLetters[currentCharPlace]);
					}
					spaceTracker++;
					addedSpace = false;
				} else if (searchCharArray(numbers, string.charAt(i)) >= 0){
					resultString.append(string.charAt(i));
					spaceTracker++;
					addedSpace = false;
				}

				if(spaceTracker % 5 == 0 && !addedSpace && i < string.length() - 2){
					resultString.append(' ');
					addedSpace = true;
				}

			}

			return resultString.toString();
		}
		public static int searchCharArray(char[] array, char searchChar){
			for (int i = 0; i < array.length; i++) {
				if (array[i] == searchChar) {
					return i;
				}
			}
			return -1;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			char[] letters = {'A', 'a', 'B', 'b', 'C', 'c', 'D',
					'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h',
					'I', 'i', 'J', 'j', 'K', 'k','L', 'l', 'M',
					'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q',
					'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V',
					'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'};
			char[] reverseLetters = {'Z','z','Y','y','X','x','W',
					'w','V','v','U','u','T','t','S','s','R','r', 'Q','q',
					'P','p','O','o','N','n','M','m','L', 'l','K','k','J',
					'j','I','i','H','h','G','g', 'F','f', 'E', 'e', 'D',
					'd', 'C', 'c', 'B', 'b', 'A', 'a'};

			char[] numbers = {'0', '1', '2', '3', '4' ,'5', '6', '7', '8', '9'};

			StringBuilder resultString = new StringBuilder();
			for(int i = 0; i < string.length(); i++){
				int currentCharPlace = searchCharArray(letters, string.charAt(i));

				if(currentCharPlace >= 0){
					resultString.append(reverseLetters[currentCharPlace]);
				} else if (searchCharArray(numbers, string.charAt(i)) >= 0){
					resultString.append(string.charAt(i));
				}
			}
			return resultString.toString();
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		List<Integer> isbnArray = new ArrayList<Integer>();
		char[] numbers = {'0', '1', '2', '3', '4' ,'5', '6', '7', '8', '9'};
		for(int i = 0; i < string.length(); i++){
			if(searchCharArray(numbers, string.charAt(i))){
				isbnArray.add((int) string.charAt(i));
			} else if(string.charAt(i) == 'X'){
				isbnArray.add(10);
			} else if(string.charAt(i) == '-'){
				continue;
			} else{
				return false;
			}
		}

		if(isbnArray.size() != 10) return false;

		int isbnChecker = (isbnArray.get(0) * 10 + isbnArray.get(1) * 9 + isbnArray.get(2) * 8 +
				isbnArray.get(3) * 7 + isbnArray.get(4) * 6 + isbnArray.get(5) * 5 +
				isbnArray.get(6) * 4 + isbnArray.get(7) * 3 + isbnArray.get(8) * 2 +
				isbnArray.get(9)) % 11;

		System.out.println(isbnChecker);
		System.out.println(isbnArray.get(9));

		return isbnChecker == 0;
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration
		char[] letters = {'a', 'b', 'c', 'd', 'e', 'f',
				'g', 'h', 'i', 'j', 'k', 'l',
				'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
				'u', 'v', 'w', 'x', 'y', 'z'};
		char[] pangramCheckerArray = {'a', 'b', 'c', 'd',
				'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
				's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

		String lowerCaseString = string.toLowerCase();

		for(int i = 0; i < string.length(); i++){

			int letterIndex = findCharIndex(letters, lowerCaseString.charAt(i));
			if(letterIndex >= 0){
				pangramCheckerArray[letterIndex] = '!';
			}
		}
		for (char c : pangramCheckerArray) {
			if (c != '!') {
				return false;
			}
		}
		return true;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		return null;
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		List<Integer> multiples = new ArrayList<>();
		int result = 0;

		for(int j = 0; j < i; j++){
			for(int k = 0; k < set.length; k++){
				if(j % set[k] == 0){
					 multiples.add(j);
					 break;
				}
			}
		}

		for(int m = 0; m < multiples.size(); m++){
			result += multiples.get(m);
		}

		return result;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		char[] numbers = {'0', '1', '2', '3', '4' ,'5', '6', '7', '8', '9'};
		boolean doubleThisOne = false;
		int luhnSum = 0;
		for(int i = string.length() - 1; i >= 0; i--){
			if(searchCharArray(numbers, string.charAt(i))){
				int currentNum = Integer.parseInt(String.valueOf(string.charAt(i)));

				if(doubleThisOne){
					int temp = currentNum * 2;
					if(temp > 10) temp -= 9;
					luhnSum += temp;
				} else {
					luhnSum += currentNum;
				}

				doubleThisOne = !doubleThisOne;
			} else if(string.charAt(i) != ' ') return false;
		}

		return luhnSum % 10 == 0;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		int firstNumber = 0;
		int secondNumber = 0;
		boolean onFirstNumber = true;
		boolean currentlyReadingNumber = false;
		String operation = "";
		StringBuilder tempNumberString1 = new StringBuilder();
		StringBuilder tempNumberString2 = new StringBuilder();
		char[] numbers = {'0', '1', '2', '3', '4' ,'5', '6', '7', '8', '9'};


		for(int i = 0; i < string.length(); i++){
			String operationStub = "";
			if(i < string.length() - 5) {
				operationStub = string.substring(i, i + 4);
			}
			if(operationStub.equals("plus")){
				operation = "plus";
			} else if(operationStub.equals("mult")){
				operation = "multiply";
			} else if(operationStub.equals("divi")){
				operation = "divide";
			} else if(operationStub.equals("minu")){
				operation = "minus";
			}

			if(onFirstNumber) {
				if (string.charAt(i) == '-') {
					tempNumberString1.append('-');
					currentlyReadingNumber = true;
				}
				if(searchCharArray(numbers, string.charAt(i))){
					tempNumberString1.append(string.charAt(i));
					currentlyReadingNumber = true;
				}
			} else {

				if (string.charAt(i) == '-') {
					tempNumberString2.append('-');
					currentlyReadingNumber = true;
				}
				if(searchCharArray(numbers, string.charAt(i))){

					tempNumberString2.append(string.charAt(i));
					currentlyReadingNumber = true;
				}
			}

			if(string.charAt(i) != '-' && !searchCharArray(numbers, string.charAt(i))){
				currentlyReadingNumber = false;
			}
			if(tempNumberString1.length() > 0 && firstNumber == 0 && !currentlyReadingNumber){
				firstNumber = Integer.parseInt(tempNumberString1.toString());
				onFirstNumber = false;
			} else if(tempNumberString2.length() > 0 && secondNumber == 0 && !currentlyReadingNumber){
				secondNumber = Integer.parseInt(tempNumberString2.toString());
			}
		}

		System.out.println(firstNumber + operation + secondNumber);
		switch(operation){
			case "plus":
				return firstNumber + secondNumber;
			case "minus":
				return firstNumber - secondNumber;
			case "multiply":
				return firstNumber * secondNumber;
			case "divide":
				return firstNumber / secondNumber;
			default:
				break;
		}

		return 0;
	}


}
